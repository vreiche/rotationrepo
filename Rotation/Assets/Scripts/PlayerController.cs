﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float speed = 5f;
	static int count;
	static int highscore = 0;
	public Text countText;
	public Text menucountText;
	public Text highscoreText;
	public GameObject gameOver;

	void Start () {
		gameOver.SetActive(false);
		count = 0;
		SetCountText();
		highscore = PlayerPrefs.GetInt("Highscore", 0);
	}

	void Update () {
		transform.Translate (transform.right * Time.deltaTime  * speed);
		//Move Object Forward
		Yup();
		//Rotation object
		dathighscore();
	}

	void Yup(){
		if(Input.touchCount > 0) {
			Touch touch = Input.GetTouch (0);
			if (touch.position.x < Screen.width/2)
				//Left Side of the Screen
			{
				transform.Rotate(4,0,0);
				//Rotates left
			}
			else if (touch.position.x > Screen.width/2){
				//Right Side of the Screen
				transform.Rotate(-4,0,0);
				//Rotates right
			}
		}
	}

	void OnTriggerEnter(Collider myCol)
	{
		if (myCol.gameObject.tag == "Destroy") 
		{
			count = count + 1;	
			SetCountText();

		}
		if (myCol.gameObject.tag == "Bad") 
		{
			countText.enabled = false;
			SetCountText();
			dathighscore();
			Time.timeScale = 0f;
			turnmenuon();
		}
	}

	void SetCountText() 
	{
		countText.text = count.ToString ();
		menucountText.text = "Current: " + count.ToString ();
		highscoreText.text = "Highscore:" + highscore.ToString();
	}

	void dathighscore() 
	{
		if (count > highscore) 
		{
			highscore = count;
			PlayerPrefs.SetInt("Highscore", highscore);
			//highscoreText.text = "Highscore:" + highscore;
		}
	}

	public void turnmenuon() {
		gameOver.SetActive(true);
	}

	void OnDestroy(){
		PlayerPrefs.SetInt("Highscore", highscore);
	}
}
