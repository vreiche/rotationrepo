﻿using UnityEngine;
using System.Collections;

public class follow : MonoBehaviour
{
	public Transform targetToFollow;
	public Vector3 targetOffset;

	private void LateUpdate()
	{
		transform.position = targetToFollow.position + targetOffset;
	}
}
