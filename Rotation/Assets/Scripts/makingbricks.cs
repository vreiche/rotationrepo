﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class makingbricks : MonoBehaviour {

	public float mydist;
	public int toprange;
	Vector3 moveDist;
	List<GameObject> demprefabs = new List<GameObject>();
	public GameObject prettyfab1;
	public GameObject prettyfab2;
	public GameObject prettyfab3;
	public GameObject prettyfab4;
	public GameObject prettyfab5;
	public GameObject prettyfab6;
	public GameObject prettyfab7;
	public GameObject prettyfab8;
	public GameObject prettyfab9;
	public GameObject prettyfab10;
	public GameObject prettyfab11;
	public int prefabrandomizer;

	void Start () {
		moveDist = new Vector3(mydist,0,0);
		demprefabs.Add(prettyfab1);
		demprefabs.Add(prettyfab2);
		demprefabs.Add(prettyfab3);
		demprefabs.Add(prettyfab4);
		demprefabs.Add(prettyfab5);
		demprefabs.Add(prettyfab6);
		demprefabs.Add(prettyfab7);
		demprefabs.Add(prettyfab8);
		demprefabs.Add(prettyfab9);
		demprefabs.Add(prettyfab10);
		demprefabs.Add(prettyfab11);
		StartCoroutine(Create());
	}
		
	IEnumerator Create(){
		while (true) {
			prefabrandomizer = UnityEngine.Random.Range(0,toprange);
			Instantiate(demprefabs[prefabrandomizer], transform.position + moveDist, Quaternion.identity);
			yield return new WaitForSeconds(1);
		}
	}
}
