﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class gameover : MonoBehaviour {

	public string levelname;
	public string menu;

	void Start () 
	{
	}

	public void restart () {
		SceneManager.LoadScene (levelname);
		Time.timeScale = 1.0f;
	}
		
	public void Menu () 
	{
		SceneManager.LoadScene (menu);
	}
}
